# Wifi QR Code
![Screenshot_01](https://gitlab.gnome.org/glerro/gnome-shell-extension-wifiqrcode/-/raw/main/assets/Screenshot_01.webp)

![Screenshot_02](https://gitlab.gnome.org/glerro/gnome-shell-extension-wifiqrcode/-/raw/main/assets/Screenshot_02.webp)

This extension add a switch to the WiFi menu, in the GNOME system menu, that show a QR Code of the active connection.

This can be useful for quickly connecting devices capable of reading QR Code and applying the settings to the system, without having to type in the name and the password of the WiFi. (e.g. Android Smartphone).

## Features
 * Add a switch to the WiFi menu, in the GNOME system menu, that show a QR Code of the active connection.
 * Support WiFi Security WEP, WPA/WPA2 Personal, WPA3 Personal and WiFi with no security.
 * Support Hidden WiFi.
 * Support Gnome Shell Versions 40, 41, 42, 43, 44, 45, 46, 47.
 * Added a functionality to copy the QR Code to clipboard with right click on it.
 * Translatable.

## Installation
Normal users are recommended to get the extension from

[![Get it on GNOME Extensions](https://raw.githubusercontent.com/andyholmes/gnome-shell-extensions-badge/master/get-it-on-ego.svg?sanitize=true){height=100px}](https://extensions.gnome.org/extension/5416/wifi-qrcode/)

## Manual Installation
You can check out the latest version with git, build and install it with [Meson](https://mesonbuild.com/).

You must install git, meson, ninja.

For a regular use and local installation these are the steps:

```bash
git clone https://gitlab.gnome.org/glerro/gnome-shell-extension-wifiqrcode.git
cd gnome-shell-extension-wifiqrcode
meson setup --prefix=$HOME/.local _build
meson install -C _build
```

Otherwise you can run the `./local_install.sh` script, it performs the build steps specified in the
previous section.

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>)
after that, or logout and login again.

Finally after restarting Gnome Shell or logging in, you need to enable the extension:

```bash
gnome-extensions enable wifiqrcode@glerro.gnome.gitlab.io
```

## Export extension ZIP file
To create a ZIP file with the extension, just run:

```bash
git clone https://gitlab.gnome.org/glerro/gnome-shell-extension-wifiqrcode.git
cd gnome-shell-extension-wifiqrcode
./export-zip.sh
```

This will create the file `wifiqrcode@glerro.gnome.gitlab.io.shell-extension.zip` with the extension, you can install it with this steps:

```bash
gnome-extensions install wifiqrcode@glerro.gnome.gitlab.io.shell-extension.zip
```

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>) after that, or logout and login again.

Finally after restarting Gnome Shell or logging in, you need to enable the extension:
```bash
gnome-extensions enable lgbutton@glerro.gnome.gitlab.io
```

## Translating

To contribute translations you will need a Gnome GitLab account, git, meson, ninja, xgettext and a translation program like Gtranslator.

Start by creating a fork on Gnome GitLab.

Clone your fork with git and setup a project with meson:

```bash
git clone https://gitlab.gnome.org/<your username>/gnome-shell-extension-wifiqrcode.git
cd gnome-shell-extension-wifiqrcode
meson setup --prefix=$HOME/.local _build
meson compile -C _build
```

Ensure the translation template is updated:

```bash
meson compile -C _build gnome-shell-extension-wifiqrcode-pot
```

Add the [language code](https://www.gnu.org/software/gettext/manual/html_node/Language-Codes.html) for your translation in /po/LINGUAS file.

Create the /po/\<language code\>.po file:

```bash
meson compile -C _build gnome-shell-extension-wifiqrcode-update-po
```

Open /po/\<language code\>.po whith Gtranslator or a similar program, translate all messages and save.

To test your translation, install the extension and restart GNOME Shell:

```bash
meson setup --prefix=$HOME/.local _build --reconfigure
meson install -C _build
```

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>)
after that, or logout and login again.

When you're happy with your translation, commit the changes and push them to your fork:

```bash
git add po/LINGUAS po/<language code>.po
git commit -m "New translations (<language>)"
git push
```

Finally, open a New Merge Request from your fork at https://gitlab.gnome.org/<your_username>/gnome-shell-extension-wifiqrcode.

## Acknowledgments
This project use the [QR Code generator library](https://www.nayuki.io/page/qr-code-generator-library) from Project Nayuki, released with MIT License.

## Donating 💳️
If you like my work and want to support it, consider [donating](https://ko-fi.com/glerro). 🙂️ Thanks!

## License
Wifi QR Code - Copyright (c) 2021-2024 Gianni Lerro {glerro} ~ <glerro@pm.me>

Wifi QR Code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wifi QR Code is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Wifi QR Code. If not, see <https://www.gnu.org/licenses/>.

